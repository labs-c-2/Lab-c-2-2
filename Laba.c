#define _USE_MATH_DEFINES
#include <stdio.h>              
#include <math.h>

float calculator(int function, float a, float x);
float getUserInput(char* title);
float calcG(float a, float x);
float calcF(float a, float x);
float calcY(float a, float x);

void main()
{
    float f = getUserInput("Select formula:\n 1 - G\n 2 - F\n 3 - Y\n ");
    float a = getUserInput("Enter a: \n");
    float x = getUserInput("Enter x: \n");

    float res = calculator(f, a, x);

    printf("Result = %f\n", res);
}

float calculator(int function, float a, float x) 
{
    switch (function)
    {
    case 1:
        return calcG(a, x);  
        break;
    case 2:
        return calcF(a, x);
        break;
    case 3:
        return calcY(a, x);
        break;
    default: break;
    }
}
 
float calcG(float a, float x)
{
    return (10 * (-45 * pow(a, 2) + 49 * a * x + 6 * pow(x, 2))) / (15 * pow(a, 2) + 49 * a * x + 24 * pow(x, 2));
}

float calcF(float a, float x)
{
    return tan((5 * pow(a, 2) + 34 * a * x + 45 * pow(x, 2)) * M_PI / 180);
}

float calcY(float a, float x)
{
    return -1 * asin((7 * pow(a, 2) - a * x - 8 * pow(x, 2)) * M_PI / 180);
}

float getUserInput(char* title) 
{
    printf(title);
    float p;
    scanf_s("%f", &p);
    return p;
}